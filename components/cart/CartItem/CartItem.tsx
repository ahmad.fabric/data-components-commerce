import cn from 'classnames'
import Image from 'next/image'
import Link from 'next/link'
import s from './CartItem.module.css'
import { useUI } from '@components/ui/context'
import usePrice from '@framework/product/use-price'
import Quantity from '@components/ui/Quantity'
import { CartItem as LineItemT } from '@fabric2/storefront-core'
import {
  $UpdateCartItemQuantity,
  $RemoveCartItem,
} from '@components/controllers/cart'

type ItemOption = {
  name: string
  nameId: number
  value: string
  valueId: number
}

const CartItem = ({ item, ...rest }: { item: LineItemT }) => {
  const { closeSidebarIfPresent } = useUI()

  const { price } = usePrice({
    amount: item.price!.base - (item.price?.discount?.discountAmount || 0),
    baseAmount: item.price!.base,
    currencyCode: item.price!.currency,
  })

  // TODO: Add a type for this
  const options = (item as any).options

  return (
    <li
      className={cn(s.root, {
        'opacity-50 pointer-events-none': false,
      })}
      {...rest}
    >
      <div className="flex flex-row space-x-4 py-4">
        <div className="w-16 h-16 bg-violet relative overflow-hidden cursor-pointer z-0">
          <Link href={`/product/${item.sku}`}>
            <a>
              <Image
                onClick={() => closeSidebarIfPresent()}
                className={s.productImage}
                width={150}
                height={150}
                src={item.primaryImage!.imageURL}
                alt={item.primaryImage!.altLabel}
                unoptimized
              />
            </a>
          </Link>
        </div>

        <div className="flex-1 flex flex-col text-base">
          <Link href={`/product/${item.sku}`}>
            <a>
              <span
                className={s.productName}
                onClick={() => closeSidebarIfPresent()}
              >
                {item.title}
              </span>
            </a>
          </Link>
        </div>
        <div className="flex flex-col justify-between space-y-2 text-sm">
          <span>{price}</span>
        </div>
      </div>
      <$UpdateCartItemQuantity>
        {({
          quantity,
          increaseQuantity,
          decreaseQuantity,
          updateQuantity,
          meta,
        }) => (
          <$RemoveCartItem>
            {({ removeLineItem, meta: removeItemMetaState }) => (
              <Quantity
                value={quantity}
                disabled={meta.isLoading || removeItemMetaState.isLoading}
                handleRemove={() => removeLineItem()}
                handleChange={({ target: { value } }) => updateQuantity(+value)}
                increase={() => increaseQuantity(1)}
                decrease={() => decreaseQuantity(1)}
              />
            )}
          </$RemoveCartItem>
        )}
      </$UpdateCartItemQuantity>
    </li>
  )
}

export default CartItem
