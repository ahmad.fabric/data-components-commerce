import { APIError } from '@fabric2/storefront-core'
import { ReactNode } from 'react'

export type ControllerChildren<T> = ReactNode | ((props: T) => ReactNode)

export type Meta = {
  isLoading: boolean
  isError: boolean
  isSuccess: boolean
  isIdle: boolean
  error: APIError | null
}

export interface CommonProps<ChildrenProps, Data = unknown, Error = APIError> {
  children?: ControllerChildren<ChildrenProps>
  onSuccess?: (data: Data) => void
  onError?: (error: Error) => void
}
