import React, { Fragment } from 'react'
import { ReactNode } from 'react'

export const renderChildren = <T extends unknown>(
  children: ReactNode | ((props: T) => any),
  props: T = {} as T
) => {
  console.log({ children })
  return (
    <Fragment>
      {children
        ? isFunction(children)
          ? children(props)
          : !isEmptyChildren(children)
          ? React.Children.only(children)
          : null
        : null}
    </Fragment>
  )
}
export const isEmptyChildren = (children: any): boolean =>
  React.Children.count(children) === 0

export const isFunction = (obj: any): obj is Function =>
  typeof obj === 'function'
