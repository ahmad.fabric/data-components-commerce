import {
  Cart,
  useCart,
  CartItem,
  useUpdateLineItems,
  useAddLineItems,
  useRemoveLineItem,
  useProductPrice,
} from '@fabric2/storefront-core'
import React, { Fragment, useContext, useEffect } from 'react'
import { CommonProps, Meta } from '../types'
import { renderChildren } from '../utils'

export const $GetCart = ({
  children,
  onError,
  onSuccess,
}: CommonProps<{ cart?: Cart; meta: Meta }, Cart>) => {
  const { data, ...meta } = useCart({ onError, onSuccess })
  const addToCartMutation = useAddLineItems()

  useEffect(() => {
    addToCartMutation.mutate({
      items: [{ itemID: 10, quantity: 12 }],
    })
  }, [])

  return renderChildren(children, { cart: data, meta })
}

const CartItemContext = React.createContext<{ item?: CartItem }>({})
export const $GetCartItems = ({
  children,
  onError,
  onSuccess,
}: CommonProps<{ item: CartItem; key: string }, CartItem[]>) => {
  const { data } = useCart({
    onError,
    onSuccess: (data) => onSuccess?.(data.items),
  })

  if (!data) {
    return null
  }

  return (
    <Fragment>
      {data?.items.map((item) => (
        <CartItemContext.Provider key={item.id} value={{ item }}>
          {renderChildren(children, {
            item,
            key: item.itemID,
          })}
        </CartItemContext.Provider>
      ))}
    </Fragment>
  )
}

export const $UpdateCartItemQuantity = ({
  children,
  onError,
  onSuccess,
}: CommonProps<{
  updateQuantity: (quantity: number) => void
  quantity: number
  increaseQuantity: (value: number) => void
  decreaseQuantity: (value: number) => void
  meta: Meta
}>) => {
  const { item } = useContext(CartItemContext)
  const { mutate, mutateAsync, ...meta } = useUpdateLineItems({
    onError,
    onSuccess,
  })
  const updateLineItems = mutateAsync
  const quantity = item?.quantity

  const updateQuantity = (quantity: number) => {
    return updateLineItems({
      items: [{ lineItemID: item?.lineItemID, itemID: item?.itemID, quantity }],
    })
  }

  const increaseQuantity = (value: number = 1) => {
    return updateQuantity(quantity + value)
  }

  const decreaseQuantity = (value: number = 1) => {
    return updateQuantity(quantity - value)
  }

  return renderChildren(children, {
    quantity,
    meta,
    updateQuantity,
    increaseQuantity,
    decreaseQuantity,
  })
}

export const $RemoveCartItem = ({
  children,
  onError,
  onSuccess,
}: CommonProps<{ removeLineItem: () => void; meta: Meta }, Cart>) => {
  const { item } = useContext(CartItemContext)
  const { mutate, ...meta } = useRemoveLineItem({ onSuccess, onError })

  const removeLineItem = () => {
    return mutate({ lineItemID: item?.lineItemID })
  }

  return renderChildren(children, { removeLineItem, meta })
}
