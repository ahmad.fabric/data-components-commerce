import type { GetStaticPropsContext } from 'next'
import useCart from '@framework/cart/use-cart'
import usePrice from '@framework/product/use-price'
import commerce from '@lib/api/commerce'
import { Layout } from '@components/common'
import { Button, Text } from '@components/ui'
import { Bag, Cross, Check, MapPin, CreditCard } from '@components/icons'
import { CartItem } from '@components/cart'
import { useFabricClient } from '@fabric2/storefront-core'
import { $GetCart, $GetCartItems } from '@components/controllers/cart'
import React, { Fragment } from 'react'

export async function getStaticProps({
  preview,
  locale,
  locales,
}: GetStaticPropsContext) {
  const config = { locale, locales }
  const pagesPromise = commerce.getAllPages({ config, preview })
  const siteInfoPromise = commerce.getSiteInfo({ config, preview })
  const { pages } = await pagesPromise
  const { categories } = await siteInfoPromise
  return {
    props: { pages, categories },
  }
}

export default function Cart() {
  return (
    <div className="grid lg:grid-cols-12 w-full max-w-7xl mx-auto">
      <$GetCart>
        {({ cart, meta }) => {
          if (meta.isLoading) {
            return <p>Loading...</p>
          }

          if (meta.error) {
            return <p>{meta?.error.message}</p>
          }

          const isEmpty = !cart?.items?.length
          const total = '200$'
          const subTotal = '190$'
          const quantity = cart?.quantity
          return (
            <Fragment>
              <div className="lg:col-span-8">
                <div className="px-4 sm:px-6 flex-1">
                  <Text variant="pageHeading">My Cart - Items: {quantity}</Text>
                  <Text variant="sectionHeading">Review your Order</Text>
                  <ul className="py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-accent-2 border-b border-accent-2">
                    <$GetCartItems>
                      {(props) => {
                        return <CartItem {...props} />
                      }}
                    </$GetCartItems>
                  </ul>
                </div>
              </div>
              <div className="lg:col-span-4">
                <div className="border-t border-accent-2">
                  <ul className="py-3">
                    <li className="flex justify-between py-1">
                      <span>Subtotal</span>
                      <span>{}</span>
                    </li>
                    <li className="flex justify-between py-1">
                      <span>Taxes</span>
                      <span>Calculated at checkout</span>
                    </li>
                    <li className="flex justify-between py-1">
                      <span>Estimated Shipping</span>
                      <span className="font-bold tracking-wide">FREE</span>
                    </li>
                  </ul>
                  <div className="flex justify-between border-t border-accent-2 py-3 font-bold mb-10">
                    <span>Total</span>
                    <span>{total}</span>
                  </div>
                </div>
                <div className="flex flex-row justify-end">
                  <div className="w-full lg:w-72">
                    {isEmpty ? (
                      <Button href="/" Component="a" width="100%">
                        Continue Shopping
                      </Button>
                    ) : (
                      <Button href="/checkout" Component="a" width="100%">
                        Proceed to Checkout
                      </Button>
                    )}
                  </div>
                </div>
              </div>
            </Fragment>
          )
        }}
      </$GetCart>
    </div>
  )
}

Cart.Layout = Layout
