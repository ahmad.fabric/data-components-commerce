import '@assets/main.css'
import '@assets/chrome-bug.css'
import 'keen-slider/keen-slider.min.css'

import { FC, useEffect } from 'react'
import type { AppProps } from 'next/app'
import { Head } from '@components/common'
import { ManagedUIContext } from '@components/ui/context'
import {
  fabricStorefront,
  StorefrontCoreProvider,
} from '@fabric2/storefront-core'

const storefrontClient = fabricStorefront.init({
  accountId: '60b910a9b9d1b20008fae769',
  stage: 'sandbox',
  baseUrl: 'https://uat01-apigw.refstorefrontdev.fabric.zone',
  storeApiBaseUrl: 'https://store-api.dev.storefront.fabric.inc',
  storeApiGraphQLEndpoint:
    'https://store-api.dev.storefront.fabric.inc/graphql/query',
  locale: 'en-US',
  environment: 'development',
})

const Noop: FC = ({ children }) => <>{children}</>

export default function MyApp({ Component, pageProps }: AppProps) {
  const Layout = (Component as any).Layout || Noop

  useEffect(() => {
    document.body.classList?.remove('loading')
  }, [])

  return (
    <>
      <Head />
      <ManagedUIContext>
        <StorefrontCoreProvider client={storefrontClient}>
          <Layout pageProps={pageProps}>
            <Component {...pageProps} />
          </Layout>
        </StorefrontCoreProvider>
      </ManagedUIContext>
    </>
  )
}
