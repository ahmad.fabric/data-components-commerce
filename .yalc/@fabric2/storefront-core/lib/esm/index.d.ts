/// <reference types="react" />
import { RequestDocument, Variables } from "graphql-request/dist/types";
import { QueryKey, UseMutationOptions, UseQueryOptions, UseMutateAsyncFunction, UseMutateFunction, UseMutationResult, UseQueryResult } from "react-query";
/**
 * @alias type StorefrontConfig
 * @memberof FabricStorefront
 */
type StorefrontConfig = {
    /**
     * Fabric api base url
     */
    baseUrl: string;
    /**
     * Fabric storefront rest api base url
     */
    storeApiBaseUrl: string;
    /**
     * Fabric storefront api graphql endpoint
     */
    storeApiGraphQLEndpoint?: string;
    /**
     * Fabric account id
     */
    accountId: string;
    /**
     * Stage (sandbox / prod01)
     */
    stage: string;
    /**
     * production | development
     */
    environment: string;
    /**
     * Locale (eg. en-US)
     */
    locale: string | undefined;
};
/* TODO(ahmad): Remove this when all the APIs are converted to GraphQL */
declare class ApiError {
    message: string | undefined;
    status?: number | undefined;
    constructor(message: string | undefined, status?: number | undefined);
}
type Extensions = Record<string, string> | undefined;
declare class GraphQLError {
    message: string;
    code: string;
    path: string[] | undefined;
    extensions: Extensions;
    constructor(message: string, code: string, path?: string[], extensions?: Extensions);
}
type Maybe<T> = T | null;
type Exact<T extends {
    [key: string]: unknown;
}> = {
    [K in keyof T]: T[K];
};
type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]?: Maybe<T[SubKey]>;
};
type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    /** Any data type */
    Any: any;
    /** Datetime in RFC3339 format */
    Time: any;
    /** Accepts unsigned integer. Negatives are not allowed */
    UInt32: any;
};
/** AddCartItem is input item for adding to cart */
type AddCartItem = {
    /** attributes are optional for now */
    attributes?: Maybe<Array<AttributeInput>>;
    itemID: Scalars["UInt32"];
    quantity: Scalars["UInt32"];
};
/**
 * AddToCartInput is input for adding items to cart.
 * Keeping cartID empty will create new cart.
 * Provide cartID for adding items to existing cart.
 */
type AddToCartInput = {
    /** cartID is for adding items to existing cart */
    cartID?: Maybe<Scalars["String"]>;
    items: Array<AddCartItem>;
};
/** Amount ... */
type Amount = {
    __typename?: "Amount";
    currency: Scalars["String"];
    currencySymbol?: Maybe<Scalars["String"]>;
    formattedSubTotal?: Maybe<Scalars["String"]>;
    formattedTotal?: Maybe<Scalars["String"]>;
    /** Total cart amount without discounts */
    subTotal: Scalars["Float"];
    /** Total cart amount after applying discounts */
    total: Scalars["Float"];
};
/** ApplyCartPromoInput is input for applying promo/coupon code to cart. */
type ApplyCartPromoInput = {
    __typename?: "ApplyCartPromoInput";
    cartID: Scalars["String"];
    promoCode: Scalars["String"];
};
/** Attribute ... */
type Attribute = {
    __typename?: "Attribute";
    attributeType: Scalars["String"];
    description: Scalars["String"];
    mapping: Scalars["String"];
    name: Scalars["String"];
    value: Scalars["Any"];
};
/** AttributeInput attributes for input */
type AttributeInput = {
    attributeType: Scalars["String"];
    description: Scalars["String"];
    mapping: Scalars["String"];
    name: Scalars["String"];
    value: Scalars["Any"];
};
/** BreadcrumbsItem category breadcrumbs item */
type BreadcrumbsItem = {
    __typename?: "BreadcrumbsItem";
    attributes: Array<Attribute>;
    id: Scalars["String"];
    name: Scalars["String"];
};
/** Cart ... */
type Cart = {
    __typename?: "Cart";
    allPromosApplied: Array<Promo>;
    amount: Amount;
    attributes: Array<Attribute>;
    cartID: Scalars["String"];
    /** time fields can be null */
    createdAt?: Maybe<Scalars["Time"]>;
    deleted: Scalars["Boolean"];
    items: Array<CartItem>;
    /** Total number of item quantities */
    quantity: Scalars["UInt32"];
    /** Whether cart belongs to registered user or not */
    registeredUser: Scalars["Boolean"];
    updatedAt?: Maybe<Scalars["Time"]>;
    userId?: Maybe<Scalars["String"]>;
};
/**
 * CartErrorEnum ...
 * TODO: should provide all error codes as enum
 */
declare enum CartErrorEnum {
    InventoryOutOfStock = "INVENTORY_OUT_OF_STOCK",
    ItemNotSellable = "ITEM_NOT_SELLABLE",
    PromoUnavailable = "PROMO_UNAVAILABLE"
}
/** CartItem ... */
type CartItem = {
    __typename?: "CartItem";
    /** Amount with total and subtotal */
    amount?: Maybe<Amount>;
    /** Total price of all selected attributes */
    attributeTotalPrice: Scalars["Float"];
    attributes: Array<Attribute>;
    /** time fields can be null */
    createdAt?: Maybe<Scalars["Time"]>;
    /** How many quantities are discounted */
    discountedQuantity: Scalars["UInt32"];
    group: Array<Scalars["String"]>;
    id: Scalars["ID"];
    inventory?: Maybe<ProductInventory>;
    /** Whether item is available for pickup */
    isPickup: Scalars["Boolean"];
    itemID: Scalars["UInt32"];
    /** ID assigned to line item in cart */
    lineItemID: Scalars["UInt32"];
    price?: Maybe<CartItemPrice>;
    priceListId?: Maybe<Scalars["String"]>;
    primaryImage?: Maybe<ProductImage>;
    quantity: Scalars["UInt32"];
    /** TODO: Clarify use case for sample field in cart */
    sample: Scalars["Boolean"];
    secondaryImages: Array<ProductImage>;
    sku?: Maybe<Scalars["String"]>;
    /**
     * TaxCode is mandatory attribute need to present for item. Though FE dont need to pass while adding.
     * Cart api validate in backend
     */
    taxCode: Scalars["String"];
    title?: Maybe<Scalars["String"]>;
    updatedAt?: Maybe<Scalars["Time"]>;
    /** Weight of item, required for add to cart for fabric checkout */
    weight: Scalars["Float"];
    weightUnit?: Maybe<Scalars["String"]>;
};
/** CartItemPrice ... */
type CartItemPrice = {
    __typename?: "CartItemPrice";
    base: Scalars["Float"];
    currency: Scalars["String"];
    discount?: Maybe<Discount>;
    sale: Scalars["Float"];
};
/** Category ... */
type Category = {
    __typename?: "Category";
    attributes: Array<Attribute>;
    breadcrumbs: Array<BreadcrumbsItem>;
    id: Scalars["String"];
    name: Scalars["String"];
};
/**  CategoryAttribute - this is a category tree attribute  */
type CategoryAttribute = {
    __typename?: "CategoryAttribute";
    description: Scalars["String"];
    id: Scalars["ID"];
    name: Scalars["String"];
    type: Scalars["String"];
    value: Scalars["Any"];
};
/**  CategoryTree - this is the root type for querying category tree  */
type CategoryTree = {
    __typename?: "CategoryTree";
    attributes: Array<CategoryAttribute>;
    children: Array<CategoryTree>;
    id: Scalars["ID"];
    name: Scalars["String"];
};
/** DataTypeEnum are available data types */
declare enum DataTypeEnum {
    Boolean = "BOOLEAN",
    Datetime = "DATETIME",
    Decimal = "DECIMAL",
    Integer = "INTEGER",
    Serial = "SERIAL",
    Text = "TEXT"
}
/** Discount - applicable against price */
type Discount = {
    __typename?: "Discount";
    /** Discount given */
    discountAmount: Scalars["Float"];
    /** Price after discount */
    price: Scalars["Float"];
    /** Promos/Offers applied to price */
    promosApplied: Array<Promo>;
};
/** FacetFilterType determines whether search facet is range filter or list of values filter */
declare enum FacetFilterTypeEnum {
    ListOfValues = "LIST_OF_VALUES",
    RangeFilter = "RANGE_FILTER"
}
/**
 * FacetValueCount is value of facet with its product counts.
 * Ex. for color facet, {value=Red, productCount=10}, {value=Black, productCount=5}
 */
type FacetValueCount = {
    __typename?: "FacetValueCount";
    productCount: Scalars["Int"];
    value: Scalars["Any"];
};
/** Mutation contains all mutation methods for graphql  */
type MutationAddToCartArgs = {
    input: AddToCartInput;
};
/** Mutation contains all mutation methods for graphql  */
type MutationEmptyCartByIdArgs = {
    id: Scalars["ID"];
};
/** Mutation contains all mutation methods for graphql  */
type MutationRemoveCartItemArgs = {
    input: RemoveCartItemInput;
};
/** Mutation contains all mutation methods for graphql  */
type MutationUpdateCartArgs = {
    input: UpdateCartInput;
};
/** PageArgs are argumets for pagination */
type PageArgs = {
    /** after return the records after this token */
    after?: Maybe<Scalars["String"]>;
    /** first refers to the limit of items to return */
    first?: Maybe<Scalars["Int"]>;
    /**
     * page returns the records of this page number.
     * page argument will be ignored if after cursor passed.
     * page starts with 0.
     */
    page?: Maybe<Scalars["Int"]>;
};
/** PageInfo represents pagination info for current page */
type PageInfo = {
    __typename?: "PageInfo";
    /** endCursor refers to the the first item of the last page */
    endCursor?: Maybe<Scalars["String"]>;
    /** hasNextPage informs if there is a next page */
    hasNextPage: Scalars["Boolean"];
    /** hasPreviousPage informs if there is a previous page */
    hasPreviousPage: Scalars["Boolean"];
    /** page refers to current page number */
    page?: Maybe<Scalars["Int"]>;
    /** startCursor refers to the start of the first page */
    startCursor?: Maybe<Scalars["String"]>;
    /** totalCount the total number of records */
    totalCount?: Maybe<Scalars["Int"]>;
};
/** Product ... */
type Product = {
    __typename?: "Product";
    /** TODO: Adding map scaler type is not supporting qurying map fields(keys). Explore this option to support it */
    attributes: Array<Attribute>;
    brand: Scalars["String"];
    bundle: Array<ProductVariant>;
    cartDetails: ProductCartDetails;
    categories: Array<Category>;
    description: Scalars["String"];
    inventory: ProductInventory;
    isDefault: Scalars["Boolean"];
    itemID: Scalars["UInt32"];
    name: Scalars["String"];
    parentSku: Scalars["String"];
    price: ProductPrice;
    primaryImage: ProductImage;
    secondaryImages: Array<ProductImage>;
    sku: Scalars["String"];
    variantAttributes: Array<VariantAttributesItem>;
    variants: Array<ProductVariant>;
};
/** ProductCartDetails cart details in product */
type ProductCartDetails = {
    __typename?: "ProductCartDetails";
    inCart: Scalars["Int"];
    /** lastAdded can be null */
    lastAdded?: Maybe<Scalars["Time"]>;
};
/** ProductConnection represents product edges with page info */
type ProductConnection = {
    __typename?: "ProductConnection";
    edges: Array<ProductEdge>;
    pageInfo: PageInfo;
};
/**
 * ProductDetailsInput input arguments for get product details (either of itemID / sku is required - when
 * both are provided - itemID is preferred)
 */
type ProductDetailsInput = {
    itemID?: Maybe<Scalars["UInt32"]>;
    sku?: Maybe<Scalars["String"]>;
};
/** ProductEdge is edge elment in product search/list with each node is product having own cursor */
type ProductEdge = {
    __typename?: "ProductEdge";
    cursor?: Maybe<Scalars["String"]>;
    node: Product;
};
/** ProductImage ... */
type ProductImage = {
    __typename?: "ProductImage";
    altLabel: Scalars["String"];
    id: Scalars["String"];
    imageURL: Scalars["String"];
    order: Scalars["Int"];
};
/** ProductInventory ... */
type ProductInventory = {
    __typename?: "ProductInventory";
    inStock: Scalars["Boolean"];
    locations: Array<ProductInventoryPerLocation>;
    totalQuantityInStock: Scalars["UInt32"];
    totalQuantityReserved: Scalars["UInt32"];
};
/** ProductInventoryPerLocation location inventory */
type ProductInventoryPerLocation = {
    __typename?: "ProductInventoryPerLocation";
    /** lastUpdatedDate can be null */
    lastUpdatedDate?: Maybe<Scalars["Time"]>;
    locationId: Scalars["String"];
    quantityInStock: Scalars["UInt32"];
    quantityInTransit: Scalars["UInt32"];
    quantityReserved: Scalars["UInt32"];
};
/** ProductPrice ... */
type ProductPrice = {
    __typename?: "ProductPrice";
    base: Scalars["Float"];
    currency: Scalars["String"];
    currencySymbol: Scalars["String"];
    priceListId: Scalars["String"];
    sale: Scalars["Float"];
};
/** ProductRangeFacet is ranage of values search facet. Ex. Price{min: 100, max: 500} */
type ProductRangeFacet = {
    __typename?: "ProductRangeFacet";
    avg: Scalars["Float"];
    filterType: FacetFilterTypeEnum;
    identifier: Scalars["String"];
    max: Scalars["Float"];
    min: Scalars["Float"];
    text: Scalars["String"];
    valueType: DataTypeEnum;
};
/** ProductSearchInput input arguments for product search */
type ProductSearchInput = {
    keyword?: Maybe<Scalars["String"]>;
    rangeFacets?: Maybe<Array<RangeFacetsInputItem>>;
    /**
     * ruleContexts are string for rules created for custom results.
     * See more at: https://www.algolia.com/doc/guides/managing-results/rules/rules-overview/how-to/customize-search-results-by-platform/
     */
    ruleContexts?: Maybe<Array<Scalars["String"]>>;
    sortBy?: Maybe<ProductsSortByEnum>;
    standardFacets?: Maybe<StandardFacetsInput>;
    valuesFacets?: Maybe<Array<ValueFacetsInputItem>>;
};
/** ProductSearchResponse response type for product search */
type ProductSearchResponse = {
    __typename?: "ProductSearchResponse";
    categories: Array<Category>;
    products: ProductConnection;
    rangeFacets: Array<ProductRangeFacet>;
    selectedFacets: ProductSearchSelectedFacets;
    valuesFacets: Array<ProductValuesFacet>;
};
/** ProductSearchResponse response type for product search */
type ProductSearchResponseProductsArgs = {
    page?: Maybe<PageArgs>;
};
/** ProductSearchSelectedFacets are all selected facets in search returned in response */
type ProductSearchSelectedFacets = {
    __typename?: "ProductSearchSelectedFacets";
    keyword: Scalars["String"];
    rangeFacets: Array<ProductRangeFacet>;
    sortBy: ProductsSortByEnum;
    standardFacets: StandardFacets;
    valuesFacets: Array<ProductValuesFacet>;
};
/** ProductValuesFacet is list of values search facet. Ex. Color[red, green] */
type ProductValuesFacet = {
    __typename?: "ProductValuesFacet";
    filterType: FacetFilterTypeEnum;
    identifier: Scalars["String"];
    text: Scalars["String"];
    valueType: DataTypeEnum;
    values: Array<FacetValueCount>;
};
/** ProductVariant ... */
type ProductVariant = {
    __typename?: "ProductVariant";
    attributes: Array<Attribute>;
    description: Scalars["String"];
    image: ProductImage;
    inventory?: Maybe<ProductInventory>;
    /** Whether this is the default variant which should be shown first. */
    isDefault: Scalars["Boolean"];
    itemID: Scalars["UInt32"];
    name: Scalars["String"];
    price?: Maybe<ProductPrice>;
    sku: Scalars["String"];
};
/** ProductsSortByEnum are avaialble sort options */
declare enum ProductsSortByEnum {
    Alphabetical = "ALPHABETICAL",
    None = "NONE",
    PriceAsc = "PRICE_ASC",
    PriceDesc = "PRICE_DESC"
}
/** Promo ... */
type Promo = {
    __typename?: "Promo";
    /** internal id */
    id?: Maybe<Scalars["String"]>;
    on?: Maybe<PromoAppliedOn>;
    promoCode?: Maybe<Scalars["Any"]>;
    /** Promo ID used in operations */
    promoId?: Maybe<Scalars["String"]>;
    /** Whether percentage, amount etc */
    unit?: Maybe<Scalars["String"]>;
    value?: Maybe<Scalars["Any"]>;
};
/** PromoAppliedOn ... */
type PromoAppliedOn = {
    __typename?: "PromoAppliedOn";
    /** Applied on SKU, quantity, total price etc */
    kind: Scalars["String"];
    value?: Maybe<Scalars["Any"]>;
};
/** Query contains all query methods(queries) for graphql  */
type QueryCategoryTreeArgs = {
    id: Scalars["ID"];
};
/** Query contains all query methods(queries) for graphql  */
type QueryGetCartByIdArgs = {
    id: Scalars["ID"];
};
/** Query contains all query methods(queries) for graphql  */
type QueryGetProductDetailsArgs = {
    input: ProductDetailsInput;
};
/** Query contains all query methods(queries) for graphql  */
type QuerySearchProductsArgs = {
    input: ProductSearchInput;
};
/** RangeFacetsInputItem range facets for product search */
type RangeFacetsInputItem = {
    identifier: Scalars["String"];
    max: Scalars["Float"];
    min: Scalars["Float"];
};
/** RemoveCartItemActionEnum are what action should perform for removing cart item */
declare enum RemoveCartItemActionEnum {
    Delete = "DELETE",
    MoveToWishlist = "MOVE_TO_WISHLIST"
}
/**
 * RemoveCartItemInput is input for removing item from cart.
 * Can provide action to whether delete and move to wishlist or just delete from cart
 * Note: Move to wishlist not supported yet
 */
type RemoveCartItemInput = {
    /**
     * action what action should perform for removing cart item.
     * Default is delete from cart
     * Note: Move to wishlist not supported yet
     */
    action?: Maybe<RemoveCartItemActionEnum>;
    cartID: Scalars["String"];
    lineItemID: Scalars["UInt32"];
};
/** StandardFacets ... */
type StandardFacets = {
    __typename?: "StandardFacets";
    brands: Array<Scalars["String"]>;
    category: Scalars["String"];
    hasInventory: Scalars["Boolean"];
    hasSubscription: Scalars["Boolean"];
    maxPrice: Scalars["Float"];
    minPrice: Scalars["Float"];
    onSale: Scalars["Boolean"];
    showFeatured: Scalars["Boolean"];
};
/** StandardFacetsInput standard facets for product search */
type StandardFacetsInput = {
    brands?: Maybe<Array<Scalars["String"]>>;
    category?: Maybe<Scalars["String"]>;
    hasInventory?: Maybe<Scalars["Boolean"]>;
    hasSubscription?: Maybe<Scalars["Boolean"]>;
    maxPrice?: Maybe<Scalars["Float"]>;
    minPrice?: Maybe<Scalars["Float"]>;
    onSale?: Maybe<Scalars["Boolean"]>;
    showFeatured?: Maybe<Scalars["Boolean"]>;
};
/**
 * UpdateCartInput is input for updating existing items in cart.
 * Only privided items will be updated. Other items will remain as it is in cart
 */
type UpdateCartInput = {
    cartID: Scalars["String"];
    items: Array<UpdateCartItem>;
};
type UpdateCartItem = {
    /** attributes are optional for now */
    attributes?: Maybe<Array<AttributeInput>>;
    itemID: Scalars["UInt32"];
    lineItemID: Scalars["UInt32"];
    quantity: Scalars["UInt32"];
};
/** ValueFacetsInputItem  values factes for product search */
type ValueFacetsInputItem = {
    identifier: Scalars["String"];
    values: Array<Scalars["String"]>;
};
/** VariantAttributesItem ... */
type VariantAttributesItem = {
    __typename?: "VariantAttributesItem";
    attributeType: Scalars["String"];
    name: Scalars["String"];
};
/**
 * @memberof Page
 * @alias XmComponent
 */
type XmComponentParams = {
    [key: string]: any;
};
/**
 * @category Types
 * @memberof Page
 * @alias XmComponent
 */
type XmComponent = {
    /**
     * Component which needs to be rendered
     */
    id: string;
    /**
     * Autogenerated key to keep the list unique
     */
    key: string;
    /**
     * params which need to be passed to component
     */
    params: XmComponentParams;
};
/**
 * Key value pair of global elements indexed by id
 * @alias GlobalElements
 */
declare class GlobalElements {
    [key: string]: XmComponent;
    constructor(components: XmComponent[]);
}
/**
 * @alias Metadata
 * @memberof Page
 */
type Metadata = {
    [key: string]: string;
};
/**
 * @alias SeoFields
 * @memberof Page
 */
type SeoFields = {
    title: string;
    description: string;
    metadata: Metadata[];
};
/**
 * Fabric XM Page
 * @category Types
 * @alias Page
 */
declare class Page {
    /**
     * Name  of page
     */
    name: string;
    /**
     * Page id of page
     */
    pageId: number;
    /**
     * Version  of page
     */
    version: number;
    /**
     * Page type of page
     */
    pageType?: {
        isDefault: boolean;
        name: string;
        urlPrefix: string;
    };
    /**
     * Page url
     */
    pageUrl: string;
    /**
     * Seo fields
     * @type SeoFields
     */
    seoFields?: SeoFields;
    /**
     * Components list of XmComponent
     * @type XmComponent[]
     */
    components: XmComponent[];
    constructor(page: any, version?: any);
    asPlainObject(): any;
}
/**
 * Key value pair of menus indexed by id
 * @alias Menu
 */
interface MenuItem {
    path: string;
    params?: Param[];
    images?: any[];
    videos?: any[];
    isActive: boolean;
    _id: string;
    name: string;
    label: string;
    order: number;
    children?: this[];
}
declare class Menu {
    items: MenuItem[];
    constructor(response: any);
}
/**
 * Key value pair of menu param
 * @alias Param
 */
type Param = {
    kind: string;
    value: string;
    id: string;
};
declare class StoreApiGraphQLClient {
    private config;
    private client;
    constructor(config: StorefrontConfig);
    query<T = any, V = Variables>(document: RequestDocument, variables?: V): Promise<T>;
    mutation<T = any, V = Variables>(document: RequestDocument, variables?: V): Promise<T>;
    private makeRequest;
}
/**
 * @alias ProductOptions
 * @memberof ProductService
 */
type ProductOptions = {
    sku: string;
};
type CategoryProductsOptions = {
    categoryIds: string[];
    excludeCategoryAttributes?: boolean;
    numSkusPerCategory?: number;
};
/**
 * Products
 * @memberof FabricStorefront
 * @alias ProductService
 * @hideconstructor
 */
declare class ProductService {
    private storefrontApi;
    static DOCUMENTS: {
        GET_PRODUCT: string;
        SEARCH_PRODUCTS: string;
        GET_CATEGORY: string;
    };
    constructor(storefrontApi: StoreApiGraphQLClient);
    /**
     * Gets product by sku
     * @param options {ProductOptions}
     * @returns product
     */
    getProduct(productOptions: ProductDetailsInput): Promise<Product>;
    /**
     * searches product(s)
     * @param options {ProductSearchOptions}
     * @returns product[]
     */
    searchProducts(productSearchInput: ProductSearchInput, pagination?: PageArgs): Promise<ProductSearchResponse>;
    /**
     * Get category hierarchy
     * @param options
     * @returns category hierarchy and related list of products.
     */
    getCategoryHierarchy(id: string): Promise<CategoryTree>;
    /**
     * Gets list of products relevant to the supplied category IDs.
     * @param options
     * @returns product[]
     */
    getProductsByCategories(options: CategoryProductsOptions): Promise<{
        text: string;
        identifier: string;
        skus: Product[];
    }>;
}
/**
 * @alias PageOptions
 * @memberof XmService
 */
type PageOptions = {
    /**
     * Url without page type prefix
     */
    url: string;
};
/**
 * @alias XmData
 * @memberof XmService
 */
type XmData = {
    page?: Page;
    menus: {
        [key: string]: Menu;
    };
    globalElements?: GlobalElements;
};
interface IXmService {
    getPage(options: PageOptions): Promise<Page>;
    getGlobalElements(): Promise<GlobalElements>;
    getMenu(): Promise<Menu>;
    getXmDataForPage(options: PageOptions): Promise<XmData>;
}
interface IBaseAPI {
    post: (url: string, data?: any, params?: any, headers?: any) => Promise<any>;
    get: (url: string, params?: any, headers?: any) => Promise<any>;
    fetch: (url: string, options?: any) => Promise<any>;
}
type CommerceApiClientConfig = StorefrontConfig;
declare class CommerceApiClient implements IBaseAPI {
    private config;
    private axiosInstance;
    constructor(config: CommerceApiClientConfig);
    private enableInterceptors;
    private getSuccessResponseHandler;
    get(url: string, params?: any, headers?: any): Promise<import("axios").AxiosResponse<any>>;
    post(url: string, data?: {}, params?: {}, headers?: {}): Promise<import("axios").AxiosResponse<any>>;
    fetch(url: string, options?: {}): Promise<import("axios").AxiosResponse<any>>;
    fetchAll(values?: any[]): Promise<any[]>;
}
/**
 * Fabric XM service
 * @memberof FabricStorefront
 * @alias XmService
 * @hideconstructor
 */
declare class XmService implements IXmService {
    private commerceApi;
    static ENDPOINTS: {
        page: string;
        menu: string;
        globalElements: string;
    };
    constructor(commerceApi: CommerceApiClient);
    /**
     * Gets xm page
     * @param options {PageOptions}
     * @returns page
     * @throws {XmError}
     */
    getPage(options: PageOptions): Promise<Page>;
    /**
     * Gets global elements
     * @returns global elements
     */
    getGlobalElements(): Promise<GlobalElements>;
    /**
     * Gets xm menu
     * @returns menu
     */
    getMenu(): Promise<Menu>;
    /**
     * Gets XmPage and GlobalElements
     * @param options {PageOptions}
     * @returns XmData object which contains page and global elements {XmData}
     */
    getXmDataForPage(options: PageOptions): Promise<XmData>;
}
declare enum PromoValues {
    OFF = "%OFF",
    AMOUNT_OFF = "AMOUNT_OFF"
}
/**
 * @alias OnItem
 * @memberof Promo
 */
type OnItem = {
    kind: string;
    value: string;
};
/**
 * @alias Promo
 * @memberof AddCartItem
 */
type Promo$0 = {
    unit: string;
    value: PromoValues;
    ON: OnItem;
    id: string;
    promoId: string;
};
/**
 * @alias RemoveCartItem
 * @memberof CartService
 */
type RemoveLineItemOptions = {
    cartID: string;
    action: RemoveCartItemActionEnum;
};
/**
 * @alias CartOptions
 * @memberof CartService
 */
type GetCartOptions = {
    cartID: string;
};
/**
 * @alias UpdateLineItemsOptions
 * @memberof CartService
 */
type UpdateLineItemsOptions = {
    cartID: string;
    items: UpdateCartItem[];
};
type AddLineItemsOptions = {
    cartID: string;
    items: AddCartItem[];
};
/**
 * Cart
 * @memberof FabricStorefront
 * @alias CartService
 * @hideconstructor
 */
declare class CartService {
    private storeApiGraphQL;
    static DOCUMENTS: {
        GET_CART: string;
        UPDATE_CART: string;
        EMPTY_CART: string;
        REMOVE_CART_ITEM: string;
        ADD_TO_CART: string;
        APPLY_PROMO: string;
        REMOVE_PROMO: string;
    };
    // eslint-disable-next-line no-unused-vars
    constructor(storeApiGraphQL: StoreApiGraphQLClient);
    /**
     * Get current cart for user
     * @param options {CartOptions}
     * @returns cart
     */
    getCart(options: GetCartOptions): Promise<Cart>;
    /**
     * Add line items to cart
     * @param options {AddLineItemsOptions}
     * @returns {Promise<Cart>}
     */
    addLineItems(options: AddLineItemsOptions): Promise<Cart>;
    /**
     * update line items in cart
     * @param options {UpdateLineItemsOptions}
     * @returns {Promise<Cart>}
     */
    updateLineItems(options: UpdateLineItemsOptions): Promise<Cart>;
    /**
     * update on line item
     * @param lineItemId string
     * @param options {UpdateItemOptions}
     * @returns {Promise<Cart>}
     */
    removeLineItem(lineItemID: number, options: RemoveLineItemOptions): Promise<Cart>;
    /**
     * Empty cart by id
     * @param cartId {CartId}
     * @returns {Promise<Cart>}
     */
    emptyCart(cartID: string): Promise<Cart>;
    /**
     * Apply promo to cart
     * @param cartID string
     * @param promoId string
     * @returns {Promise<Cart>}
     */
    applyPromo(cartID: string, promoID: string): Promise<Cart>;
    /**
     * Remove an applied promo from cart
     * @param cartID string
     * @param promoId string
     * @returns {Promise<Cart>}
     */
    removePromo(cartID: string, promoID: string): Promise<Cart>;
}
/**
 * The entry point to storefront core
 * @alias FabricStorefront
 * @hideconstructor
 * @example
 *  const config = {
 *      apiBaseUrl: 'https://sandbox-apigw.fabricsales.fabric.zone'
 *      accountId: '<fabric account id>'
 *      stage: 'sandbox'
 *  }
 *  const fabric = Fabric.init(config)
 */
declare class FabricStorefront {
    private initialized;
    /**
     * Fabric XM service which supports interaction with xm.
     */
    xm: XmService;
    /**
     * Product service which fetches product with aggregated data from api-price, api-inventory etc.
     */
    product: ProductService;
    /**
     * Product service which fetches product with aggregated data from api-price, api-inventory etc.
     */
    cart: CartService;
    config: StorefrontConfig;
    constructor(config: StorefrontConfig, xm: XmService, product: ProductService, cart: CartService);
    /**
     * Initilize storefront core with environment configuration
     * @param config {StorefrontConfig}
     * @returns {FabricStorefront}
     */
    /**
     * Initilize storefront core with environment configuration
     * @param config {StorefrontConfig}
     * @returns {FabricStorefront}
     */
    static init(config: StorefrontConfig): FabricStorefront;
    init(config: StorefrontConfig): FabricStorefront;
}
declare let fabricStorefront: FabricStorefront;
type ProductPriceSchema = {
    discount?: {
        discountAmount: number;
        price: number;
    };
    base: number;
    sale: number;
    currency: string;
};
type Price = {
    amount: number | undefined;
    currency: string;
};
declare const useFormatPrice: ({ amount, currency }: Price) => string | undefined;
declare const useProductPrice: (productPrice: ProductPriceSchema) => {
    price: string;
    basePrice: string | undefined;
    discount: string | undefined;
};
/**
 * StoreMutation Options
 * @alias StoreMutationOptions
 * @typedef interface {StoreMutationOptions}
 */
interface StoreMutationOptions<TData = unknown, TError = unknown, TVariables = any, TContext = unknown> extends Pick<UseMutationOptions<TData, TError, TVariables, TContext>, `onError` | `onSettled` | `onSuccess`> {
}
/**
 * StoreQuery Options
 * @alias StoreQueryOptions
 * @typedef interface {StoreQueryOptions}
 */
interface StoreQueryOptions<TQueryFnData = unknown, TError = unknown, TData = TQueryFnData, TQueryKey extends QueryKey = QueryKey> extends Pick<UseQueryOptions<TQueryFnData, TError, TData, TQueryKey>, "enabled" | "initialData" | "onError" | "onSettled" | "onSuccess" | "select" | "refetchOnMount"> {
}
/**
 * @alias QueryResponse
 */
/**
 * gets products relevant to the provided category ids
 * @function
 * @category React Hooks
 * @param options {CategoryProductsOptions}
 * @param queryOptions {QueryOptions}
 * @alias useCategory
 */
type CategoryProductsResponse = {
    text: string;
    identifier: string;
    skus: Product[];
};
interface BasicResponse {
    failureCount: number;
    isError: boolean;
    isIdle: boolean;
    isLoading: boolean;
    isSuccess: boolean;
    status: string;
}
declare class QueryResponse<TData, TError> implements BasicResponse {
    data: TData | undefined;
    error: TError | null;
    failureCount: number;
    isError: boolean;
    isFetching: boolean;
    isIdle: boolean;
    isLoading: boolean;
    isPreviousData: boolean;
    isRefetchError: boolean;
    isSuccess: boolean;
    status: string;
    constructor(query: UseQueryResult<TData, TError>);
}
declare class MutationResponse<TData, TError, TVariables, TContext> implements BasicResponse {
    data: TData | undefined;
    error: TError | null;
    failureCount: number;
    isError: boolean;
    isIdle: boolean;
    isLoading: boolean;
    isSuccess: boolean;
    status: string;
    mutate: UseMutateFunction<TData, TError, TVariables, TContext>;
    mutateAsync: UseMutateAsyncFunction<TData, TError, TVariables, TContext>;
    constructor(mutation: UseMutationResult<TData, TError, TVariables, TContext>);
}
declare const useCategoryProducts: (options: CategoryProductsOptions, queryOptions?: StoreQueryOptions<CategoryProductsResponse, ApiError, CategoryProductsResponse, import("react-query").QueryKey> | undefined) => QueryResponse<CategoryProductsResponse, ApiError>;
/**
 * Fetches product with react query
 * @function
 * @category React Hooks
 * @param productOptions {ProductOptions}
 * @param options {QueryOptions}
 * @alias usePage
 */
declare const useProduct: (productOptions: ProductOptions, options?: StoreQueryOptions<Product, ApiError, Product, import("react-query").QueryKey> | undefined) => QueryResponse<Product, ApiError>;
/**
 * searches products
 * @function
 * @category React Hooks
 * @param searchOptions {SearchOptions}
 * @param options {QueryOptions}
 * @alias useSearchProducts
 */
type SearchOptions = {
    pagination?: PageArgs;
} & ProductSearchInput;
declare const useSearchProducts: (searchOptions: SearchOptions, options?: StoreQueryOptions<ProductSearchResponse, ApiError, ProductSearchResponse, import("react-query").QueryKey> | undefined) => QueryResponse<ProductSearchResponse, ApiError>;
/**
 * gets category
 * @function
 * @category React Hooks
 * @param categoryId {string | number}
 * @param options
 * @alias useCategory
 */
declare const useCategory: (categoryId: string, options?: StoreQueryOptions<CategoryTree, ApiError, CategoryTree, import("react-query").QueryKey> | undefined) => QueryResponse<CategoryTree, ApiError>;
/**
 * useMenu
 * @function
 * @param config {QueryConfig}
 * @alias useMenu
 */
declare const useMenu: (config: StoreQueryOptions<Menu, ApiError>) => QueryResponse<Menu, ApiError>;
/**
 * Global Elements with react query hook
 * @function
 * @category React Hooks
 * @param pageOptions {PageOptions}
 * @param options {QueryOptions}
 * @alias usePage
 * @returns {QueryResponse}
 */
declare const useGlobalElements: (options: StoreQueryOptions<GlobalElements, ApiError>) => QueryResponse<GlobalElements, ApiError>;
/**
 * usePage
 * @function
 * @param config {QueryConfig}
 * @alias usePage
 */
declare const usePage: (pageOptions: PageOptions, options: StoreQueryOptions<Page, ApiError>) => QueryResponse<Page, ApiError>;
/**
 * Refreshes cart again
 * @function
 * @category React Hooks
 * @alias useRefetchCart
 */
declare const useRefetchCart: () => () => Promise<void>;
/**
 * Fetches Cart
 * @function
 * @category React Hooks
 * @param options {StoreQueryOptions}
 * @alias useCart
 */
type Options = StoreQueryOptions<Cart, ApiError>;
declare const useCart: (options?: Options | undefined) => QueryResponse<Cart, ApiError>;
/**
 * useAddLineItems
 * @function
 * @param items {Product}
 * @param options {Options}
 * @category React Hooks
 * @alias useAddLineItems
 */
declare function useAddLineItems(options?: StoreMutationOptions<Cart, GraphQLError, {
    items: AddCartItem[];
}>): MutationResponse<Cart, GraphQLError, {
    items: AddCartItem[];
}, any>;
type Options$0 = StoreMutationOptions<Cart, ApiError, {
    lineItemID: number;
}>;
/**
 * useRemoveLineItem
 * @function
 * @param options {Options}
 * @category React Hooks
 * @alias useRemoveLineItem
 */
declare const useRemoveLineItem: (options?: Options$0 | undefined) => MutationResponse<Cart, ApiError, {
    lineItemID: number;
}, any>;
/**
 * useMoveItemToWishlist
 * @function
 * @param options {StoreMutationOptions}
 * @category React Hooks
 * @alias useMoveItemToWishlist
 */
declare const useMoveItemToWishlist: (options?: Options$0 | undefined) => MutationResponse<Cart, ApiError, {
    lineItemID: number;
}, any>;
/**
 * useResetCart - It empty the active cart
 * @function
 * @param options {Options}
 * @category React Hooks
 * @alias useResetCart
 */
type Options$1 = StoreMutationOptions<Cart, ApiError>;
declare function useResetCart(options?: Options$1): MutationResponse<Cart, ApiError, any, any>;
/**
 * useApplyPromo
 * @function
 * @param options {StoreMutationOptions}
 * @category React Hooks
 * @alias useApplyPromo
 */
declare function useApplyPromo(options?: StoreMutationOptions<Cart, ApiError, {
    promoCode: string;
}>): MutationResponse<Cart, ApiError, {
    promoCode: string;
}, any>;
/**
 * useRemovePromo
 * @function
 * @param options
 * @category React Hooks
 * @alias useRemovePromo
 */
declare function useRemovePromo(options?: StoreMutationOptions<Cart, ApiError, {
    promoID: string;
}>): MutationResponse<Cart, ApiError, {
    promoID: string;
}, any>;
/**
 * useUpdateLineItems
 * @function
 * @param options {Options}
 * @category React Hooks
 * @alias useUpdateLineItems
 */
type Options$2 = StoreMutationOptions<Cart, ApiError, {
    items: UpdateCartItem[];
}>;
declare function useUpdateLineItems(options?: Options$2): MutationResponse<Cart, ApiError, {
    items: UpdateCartItem[];
}, any>;
/**
 * Get most recently added line items
 * @function
 * @category React Hooks
 * @alias useRecentLineItems
 */
declare const useRecentLineItems: () => {
    items: CartItem[];
    clearRecentItems: () => void;
};
interface StorefrontCoreProviderProps {
    client: FabricStorefront;
}
declare const StorefrontCoreProvider: React.FC<StorefrontCoreProviderProps>;
declare const useFabricClient: () => FabricStorefront;
declare const useFabricConfig: () => StorefrontConfig;
export { FabricStorefront, fabricStorefront, ApiError, GraphQLError, Maybe, Exact, MakeOptional, MakeMaybe, Scalars, AddCartItem, AddToCartInput, Amount, ApplyCartPromoInput, Attribute, AttributeInput, BreadcrumbsItem, Cart, CartErrorEnum, CartItem, CartItemPrice, Category, CategoryAttribute, CategoryTree, DataTypeEnum, Discount, FacetFilterTypeEnum, FacetValueCount, MutationAddToCartArgs, MutationEmptyCartByIdArgs, MutationRemoveCartItemArgs, MutationUpdateCartArgs, PageArgs, PageInfo, Product, ProductCartDetails, ProductConnection, ProductDetailsInput, ProductEdge, ProductImage, ProductInventory, ProductInventoryPerLocation, ProductPrice, ProductRangeFacet, ProductSearchInput, ProductSearchResponse, ProductSearchResponseProductsArgs, ProductSearchSelectedFacets, ProductValuesFacet, ProductVariant, ProductsSortByEnum, Promo$0 as Promo, PromoAppliedOn, QueryCategoryTreeArgs, QueryGetCartByIdArgs, QueryGetProductDetailsArgs, QuerySearchProductsArgs, RangeFacetsInputItem, RemoveCartItemActionEnum, RemoveCartItemInput, StandardFacets, StandardFacetsInput, UpdateCartInput, UpdateCartItem, ValueFacetsInputItem, VariantAttributesItem, GlobalElements, Page, MenuItem, Menu, StorefrontConfig, usePage, useGlobalElements, useMenu, useCategory, useProduct, useCategoryProducts, useSearchProducts, useProductPrice, useFormatPrice, useRefetchCart, useCart, useAddLineItems, useRemoveLineItem, useUpdateLineItems, useMoveItemToWishlist, useResetCart, useApplyPromo, useRemovePromo, useRecentLineItems, StorefrontCoreProvider, useFabricClient, useFabricConfig };
//# sourceMappingURL=index.d.ts.map