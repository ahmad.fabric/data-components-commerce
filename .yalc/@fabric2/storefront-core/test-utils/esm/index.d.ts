declare const server: import("msw/node").SetupServerApi;
declare const _default: import("msw").RestHandler<import("msw").MockedRequest<import("msw").DefaultRequestBody>>[];
declare const networkErrorHandlers: import("msw").RestHandler<import("msw").MockedRequest<import("msw").DefaultRequestBody>>[];
export * from "msw";
export { _default, networkErrorHandlers, server };
//# sourceMappingURL=index.d.ts.map