#Introduction

The storefront-core is a framework created to provide developers with easy to integrate Fabric
headless services without calling the APIs directly. This framework provides abstraction by exposing
easy to use Vanilla javascript function and React Hooks. The Storefront-core can be initialised by
providing a simple config that contains the following attributes.

```js
fabricStorefront.init({
  accountId: '<fabric-account-id>',
  stage: 'sandbox',
  channel: 12,
  baseUrl: '<base-url-storefront>',
  storeApiBaseUrl: '<base-url-backend-api>',
  locale: 'en-US',
})
```

##Installation

The Storefront-core is distributed using the npm registry package. which means it can be installed
by the following command.

```
npm install @teamfabric/storefront-core --dev
```

Alternatively, the following dependency can be added package.json just as given below

```js
{
  "name": "<your-project>",
  "version": "1.1.0",
  "dependencies": {
    ...
  },
  "scripts": {
    ...
  },
  "devDependencies": {
    ...
    "@teamfabric/storefront-core": "^1.14.4",
    ...
  },
}
```

And run the following command

```
npm install
```

Once the npm install command runs successfully, you are ready to initiate to the fabricStorefront as
discussed above. After the initiation, the Storefront-core provides Vanilla Javascript function or
React Hooks. The current implementation provides access to XM, Cart and Products data.

##Plain Javascript Example

With the Storefront-core, you have the ability to call the plain javascript function which will
return a promise. This way the SDK can be used with any front end framework. To get products we can
use the following code snippet

```js
let products = []
let currentpage = 0
fabricStorefront.product.searchProducts({
  keyword:'Sofa', //search item, category, itemId
  pageSize: 20, //Number of results to be returned
  pageNumber: currentpage  // used for pagination
}).then( products => {
  products = products
  currentpage += 1
})
.catch(e: Error) {
  console.error(e)
}
```

##React Hooks

The Storefront-core also provides an implementation for the same function in hooks. All the
functions that are available in the form of plain javascript are also available in the form of React
Hooks, this makes it perfect for implementation inside dumb components and lessens the conversion
hassle.

```js
import { useSearchProducts } from '@teamfabric/storefront-core'
import React, {  useState } from 'react'

export const ProductListing = props => {
  const [query, setQuery] = useState<any>(
    {
      keyword:'Sofa', //search item, category, itemId
      pageSize: 20, //Number of results to be returned
      pageNumber: currentpage  // used for pagination
    }
  )
  const result = useSearchProducts(query as any, {
    initialData: {},
    refetchOnMount: 'always',
  })

    const data = result.data || {}
    const isLoading = result.status === 'loading'
    return (
    <div>
      <ul>
        {
          for product in result.data {
            retrun <li>product.name </li>
          }
        }
      </ul>
    </div>
  )
}
```

##Complete Documentation

This guide provides the basic introduction and usage of the Storefront-core API. Please check this
left panel for a complete list of functions and hooks available for implementation.

### Feature parity

- XM: ✅
- Products ✅
  - Search
  - By id
- Cart ✅
- Checkout
- Authentication
- Account

### Guides

- [Getting Started](/tutorial-getting-started.html)
